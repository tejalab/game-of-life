﻿using System;

namespace Game_of_Life {
    class World {
        //2-D array of Cell objects that represents the world
        public Cell[,] cells { get; set; }

        public World(int width, int height) {
            this.cells = new Cell[width, height];

            //Initialization of the world with dead cells
            for (int i = 0; i < width; i++) {
                for (int j = 0; j < height; j++) {
                    cells[i, j] = new Cell();
                }
            }
        }

        public bool isEmpty() {
            int w = cells.GetLength(0);
            int h = cells.GetLength(1);

            for (int i = 0; i < w; i++) {
                for (int j = 0; j < h; j++) {
                    //Checks if any cells are alive
                    if (cells[i, j].state == true) {
                        return false;
                    }
                }
            }

            return true;
        }

        public int getNumNeighbors(int x, int y, bool toroidal) {
            int num = 0;

            int w = cells.GetLength(0);
            int h = cells.GetLength(1);
            
            for (int i = x - 1; i <= x + 1; i++) {
                for (int j = y - 1; j <= y + 1; j++) {
                    //Makes sure we're not looking at the cell itself
                    if (!(i == x && j == y)) {
                        //Checks if the boundary condition is toroidal or finite
                        if (toroidal) {
                            //Wraps around the grid
                            int k = (i + w) % w;
                            int l = (j + h) % h;

                            //If the cell is alive, the number of neighbors is incremented
                            if (cells[k, l].state == true) {
                                num++;
                            }
                        } else {
                            //If the cell is alive and the coordinate is within the world, the number of neighbors is incremented
                            if (i < w && i > 0 && j < h && j > 0 && cells[i, j].state == true) {
                                num++;
                            }
                        }
                    }
                }
            }

            return num;
        }

        public int getNumCells() {
            int num = 0;

            int w = cells.GetLength(0);
            int h = cells.GetLength(1);

            for (int i = 0; i < w; i++) {
                for (int j = 0; j < h; j++) {
                    //If the cell is alive, the number of cells is incremented
                    if (cells[i, j].state == true) {
                        num++;
                    }
                }
            }

            return num;
        }

        public override string ToString() {
            string s = "";

            int w = cells.GetLength(0);
            int h = cells.GetLength(1);

            for (int i = 0; i < w; i++) {
                for (int j = 0; j < h; j++) {
                    //Alive cells are represented using dashes while dead cells are represented using periods
                    if (cells[i, j].state == true) {
                        s = String.Concat(s, "-");
                    } else {
                        s = String.Concat(s, ".");
                    }
                }

                //Pipes denote new rows
                s = String.Concat(s, "|");
            }

            return s;
        }
    }
}