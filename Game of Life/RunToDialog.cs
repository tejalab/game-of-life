﻿using System.Windows.Forms;

namespace Game_of_Life {
    public partial class RunToDialog : Form {
        //Allows access to the numeric up down and check box controls from Form1
        public int generation {
            get {
                return (int)numericGeneration.Value;
            }

            set {
                numericGeneration.Value = value;
            }
        }

        public int minimum {
            get {
                return (int)numericGeneration.Minimum;
            }

            set {
                numericGeneration.Minimum = value;
            }
        }

        public bool realTime {
            get {
                return checkRealTime.Checked;
            }

            set {
                checkRealTime.Checked = value;
            }
        }

        public RunToDialog() {
            InitializeComponent();
        }
    }
}
