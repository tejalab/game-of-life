﻿namespace Game_of_Life {
    partial class RunToDialog {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.buttonOK = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.numericGeneration = new System.Windows.Forms.NumericUpDown();
            this.checkRealTime = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.numericGeneration)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonOK
            // 
            this.buttonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonOK.Location = new System.Drawing.Point(136, 51);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 0;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            // 
            // buttonCancel
            // 
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(217, 51);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 1;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Generation:";
            // 
            // numericGeneration
            // 
            this.numericGeneration.Location = new System.Drawing.Point(80, 13);
            this.numericGeneration.Maximum = new decimal(new int[] {
            2147483646,
            0,
            0,
            0});
            this.numericGeneration.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericGeneration.Name = "numericGeneration";
            this.numericGeneration.Size = new System.Drawing.Size(212, 20);
            this.numericGeneration.TabIndex = 3;
            this.numericGeneration.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // checkRealTime
            // 
            this.checkRealTime.AutoSize = true;
            this.checkRealTime.Location = new System.Drawing.Point(15, 55);
            this.checkRealTime.Name = "checkRealTime";
            this.checkRealTime.Size = new System.Drawing.Size(99, 17);
            this.checkRealTime.TabIndex = 4;
            this.checkRealTime.Text = "Run in real-time";
            this.checkRealTime.UseVisualStyleBackColor = true;
            // 
            // RunToDialog
            // 
            this.AcceptButton = this.buttonOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonCancel;
            this.ClientSize = new System.Drawing.Size(304, 86);
            this.Controls.Add(this.checkRealTime);
            this.Controls.Add(this.numericGeneration);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "RunToDialog";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Run To";
            ((System.ComponentModel.ISupportInitialize)(this.numericGeneration)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numericGeneration;
        private System.Windows.Forms.CheckBox checkRealTime;
    }
}