﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace Game_of_Life {
    public partial class Form1 : Form {
        #region Global Variables

        //Settings variables
        Game_of_Life.Properties.Settings s = Properties.Settings.Default;

        bool resizable;
        int cellSize;
        int w;
        int h;
        int penSize;

        bool toroidal;

        Color cellColor;
        Color backgroundColor;
        Color gridColor;
        Color textColor;

        //Graphics variables
        float graphicsWidth;
        float graphicsHeight;
        float cellWidth;
        float cellHeight;

        int staticCellSize;

        //Game logic variables
        World universe;
        World scratchpad;
        World cache;

        int runToGeneration;
        int generation;
        int seed;

        //UI variables
        bool paused;
        bool inMenu;
        bool cacheEmpty;
        bool seeded;

        #endregion

        #region Initialization

        public Form1() {
            InitializeComponent();

            //Initialization of the settings variables with the default settings
            resizable = s.resizable;
            cellSize = s.cellSize;
            w = s.width;
            h = s.height;
            penSize = s.penSize;

            toroidal = s.toroidal;

            cellColor = s.cellColor;
            backgroundColor = s.backgroundColor;
            gridColor = s.gridColor;
            textColor = s.textColor;

            //Initialization of the game logic variables
            universe = new World(w, h);

            runToGeneration = 0;
            generation = 0;
            seed = new Random().Next();

            //Initialization of the UI variables
            paused = true;
            inMenu = false;
            cacheEmpty = true;
            seeded = false;

            //Initializes the window
            checkForResize();

            //Initialization of the timer
            if (s.fps) {
                timer.Interval = 1000 / s.speed;
            } else {
                timer.Interval = s.speed;
            }

            timer.Start();

            //Initialization of the colors for the snake game
            snakeColor = s.snakeColor;
            powerUpColor = s.powerUpColor;

            setSnakeColor.BackColor = snakeColor;
            setPowerUpColor.BackColor = powerUpColor;
        }

        #endregion

        #region Draw Function

        private void graphicsPanel_Paint(object sender, PaintEventArgs e) {
            //Paints the background of the graphics panel
            graphicsPanel.BackColor = backgroundColor;

            //Updates the graphics variables
            graphicsWidth = graphicsPanel.Width - penSize;
            graphicsHeight = graphicsPanel.Height - penSize;

            //Checks for if the grid is set to visible in the view menu and updates the graphics variables accordingly
            if (grid.Checked) {
                cellWidth = graphicsWidth / w;
                cellHeight = graphicsHeight / h;
                staticCellSize = cellSize + penSize;
            } else {
                cellWidth = (float)graphicsPanel.Width / w;
                cellHeight = (float)graphicsPanel.Height / h;
                staticCellSize = cellSize;
            }

            //Makes accessing e.Graphics simpler
            Graphics g = e.Graphics;
            //Initialization of the brush object that's used to fill in alive cells
            Brush cellBrush = new SolidBrush(cellColor);

            //Calls respective functions to fill in alive cells
            if (resizable) {
                drawResizableCells(g, cellBrush);
            } else {
                drawStaticCells(g, cellBrush);
            }

            //Clean up of the brush object
            cellBrush.Dispose();

            //Checks for if the grid is set to visible in the view menu
            if (grid.Checked) {
                //Initialization of the grid pen
                Pen gridPen = new Pen(gridColor, this.penSize);
                //Creation of an offset to draw the outline of the grid in the correct place
                int penSizeOffset = this.penSize / 2;

                //Calls respective functions to draw the grid
                if (resizable) {
                    drawResizableGrid(g, gridPen, penSizeOffset);
                } else {
                    drawStaticGrid(g, gridPen, penSizeOffset);
                }

                //Clean up of the pen object
                gridPen.Dispose();
            }

            //Checks for if the neighbor count is set to visible in the view menu
            if (neighborCount.Checked) {
                //Initialization of the brush object that's used to write a cell's neighbor count
                Brush textBrush = new SolidBrush(textColor);

                //Creates the string format to center the neighbor count within the cell
                StringFormat format = new StringFormat();
                format.Alignment = StringAlignment.Center;
                format.LineAlignment = StringAlignment.Center;

                //Calls respective functions to draw the neighbor counts
                if (resizable) {
                    drawResizableNeighborCount(g, textBrush, format);
                } else {
                    drawStaticNeighborCount(g, textBrush, format);
                }

                //Clean up of the brush object
                textBrush.Dispose();
            }
        }

        #endregion

        #region Draw Grid

        private void drawResizableGrid(Graphics g, Pen gridPen, int penSizeOffset) {
            //Draws the outline of the grid
            g.DrawRectangle(gridPen, penSizeOffset, penSizeOffset, graphicsWidth, graphicsHeight);

            //Draws the vertical lines of the grid
            for (int i = 1; i < w; i++) {
                float x = (cellWidth * i) + penSizeOffset;
                float y = graphicsHeight;

                g.DrawLine(gridPen, x, penSize, x, y);
            }

            //Draws the horizontal lines of the grid
            for (int j = 1; j < h; j++) {
                float x = graphicsWidth;
                float y = (cellHeight * j) + penSizeOffset;

                g.DrawLine(gridPen, penSize, y, x, y);
            }
        }

        private void drawStaticGrid(Graphics g, Pen gridPen, int penSizeOffset) {
            //Sets constants to allow for translation from graphics panel dimensions to window dimensions
            const int widthOffset = 16;
            const int heightOffset = 110;

            int graphicsWidth = staticCellSize * w + penSize;
            int graphicsHeight = staticCellSize * h + penSize;

            //Scales the window according to the cell size and the dimensions of the grid from options
            this.Size = new Size(graphicsWidth + widthOffset, graphicsHeight + heightOffset);

            graphicsWidth = graphicsPanel.Width - penSize;
            graphicsHeight = graphicsPanel.Height - penSize;

            //Draws the outline of the grid
            g.DrawRectangle(gridPen, penSizeOffset, penSizeOffset, graphicsWidth, graphicsHeight);

            //Draws the vertical lines of the grid
            for (int i = 1; i < w; i++) {
                int x = (staticCellSize * i) + penSizeOffset;
                int y = graphicsHeight;

                g.DrawLine(gridPen, x, penSize, x, y);
            }

            //Draws the horizontal lines of the grid
            for (int j = 1; j < h; j++) {
                int x = graphicsWidth;
                int y = (staticCellSize * j) + penSizeOffset;

                g.DrawLine(gridPen, penSize, y, x, y);
            }
        }

        #endregion

        #region Click Grid

        private Cell clickResizableGrid(MouseEventArgs e) {
            if (grid.Checked) {
                //Checks for if the user clicked outside of a cell
                for (int i = 0; i < this.penSize; i++) {
                    if ((int)(e.X % cellWidth) == i || (int)(e.Y % cellHeight) == i) {
                        return null;
                    }
                }
            }

            //Translates coordinates back to cell location in grid
            int x = (int)(e.X / cellWidth);
            int y = (int)(e.Y / cellHeight);

            //Returns reference to cell whose state needs to be changed
            return universe.cells[x, y];
        }

        private Cell clickStaticGrid(MouseEventArgs e) {
            if (grid.Checked) {
                //Checks for if the user clicked outside of a cell
                for (int i = 0; i < penSize; i++) {
                    if (e.X % staticCellSize == i || e.Y % staticCellSize == i) {
                        return null;
                    }
                }
            }

            //Translates coordinates back to cell location in grid
            int x = e.X / staticCellSize;
            int y = e.Y / staticCellSize;

            //Returns reference to cell whose state needs to be changed
            return universe.cells[x, y];
        }

        #endregion

        #region Draw Cells

        private void drawResizableCells(Graphics g, Brush cellBrush) {
            //Iterates through the universe to look for alive cells
            for (int i = 0; i < w; i++) {
                for (int j = 0; j < h; j++) {
                    //Checks if the cell is alive
                    if (universe.cells[i, j].state == true) {
                        //Float Rectangle is used to fill in the cell
                        RectangleF cell = new RectangleF(cellWidth * i, cellHeight * j, cellWidth, cellHeight);
                        g.FillRectangle(cellBrush, cell);
                    }
                }
            }
        }

        private void drawStaticCells(Graphics g, Brush cellBrush) {
            //Iterates through the universe to look for alive cells
            for (int i = 0; i < w; i++) {
                for (int j = 0; j < h; j++) {
                    //Checks if the cell is alive
                    if (universe.cells[i, j].state == true) {
                        //Resizes the window if view grid is unchecked
                        if (!grid.Checked) {
                            //Sets constants to allow for translation from graphics panel dimensions to window dimensions
                            const int widthOffset = 16;
                            const int heightOffset = 110;

                            int graphicsWidth = staticCellSize * w;
                            int graphicsHeight = staticCellSize * h;

                            //Scales the window according to the cell size and the dimensions of the grid from options
                            this.Size = new Size(graphicsWidth + widthOffset, graphicsHeight + heightOffset);
                        }

                        //Rectangle is used to fill in the cell
                        Rectangle cell = new Rectangle(staticCellSize * i, staticCellSize * j, staticCellSize, staticCellSize);
                        g.FillRectangle(cellBrush, cell);
                    }
                }
            }
        }

        #endregion

        #region Draw Neighbor Count

        private void drawResizableNeighborCount(Graphics g, Brush textBrush, StringFormat format) {
            //Iterates through the universe
            for (int i = 0; i < w; i++) {
                for (int j = 0; j < h; j++) {
                    int neighborCount = universe.getNumNeighbors(i, j, toroidal);

                    //Makes sure we only display the count for cells that have at least 1 neighbor
                    if (neighborCount > 0) {
                        float fontSize;

                        //Resizes the text according to the smaller of the two dimensions of the cell and whether or not the grid is set to visible in the view menu
                        if (cellWidth <= cellHeight) {
                            if (grid.Checked) {
                                fontSize = (cellWidth - penSize) * 7 / 10;
                            } else {
                                fontSize = cellWidth * 7 / 10;
                            }
                        } else {
                            if (grid.Checked) {
                                fontSize = (cellHeight - penSize) * 7 / 10;
                            } else {
                                fontSize = cellHeight * 7 / 10;
                            }
                        }

                        Font font = new Font("Century Gothic", fontSize, GraphicsUnit.Pixel);

                        //Float Rectangle is used as a container for the text
                        RectangleF cell;

                        //Sizes the rectangle according to whether or not the grid is set to visible in the view menu
                        if (grid.Checked) {
                            cell = new RectangleF(cellWidth * i + penSize, cellHeight * j + penSize, cellWidth - penSize, cellHeight - penSize);
                        } else {
                            cell = new RectangleF(cellWidth * i, cellHeight * j, cellWidth, cellHeight);
                        }

                        g.DrawString(neighborCount.ToString(), font, textBrush, cell, format);
                    }
                }
            }
        }

        private void drawStaticNeighborCount(Graphics g, Brush textBrush, StringFormat format) {
            //Iterates through the universe
            for (int i = 0; i < w; i++) {
                for (int j = 0; j < h; j++) {
                    int neighborCount = universe.getNumNeighbors(i, j, toroidal);

                    //Makes sure we only display the count for cells that have at least 1 neighbor
                    if (neighborCount > 0) {
                        float fontSize = cellSize * 7 / 10;
                        Font font = new Font("Century Gothic", fontSize, GraphicsUnit.Pixel);

                        //Rectangle is used as a container for the text
                        Rectangle cell;

                        //Sizes the rectangle according to whether or not the grid is set to visible in the view menu
                        if (grid.Checked) {
                            cell = new Rectangle(staticCellSize * i + penSize, staticCellSize * j + penSize, cellSize, cellSize);
                        } else {
                            cell = new Rectangle(staticCellSize * i, staticCellSize * j, staticCellSize, staticCellSize);
                        }

                        g.DrawString(neighborCount.ToString(), font, textBrush, cell, format);
                    }
                }
            }
        }

        #endregion

        #region Game Logic

        private void advanceGeneration() {
            //Clears the scratchpad
            scratchpad = new World(w, h);

            for (int i = 0; i < w; i++) {
                for (int j = 0; j < h; j++) {
                    Cell presentCell = universe.cells[i, j];
                    int generation = presentCell.generation + 1;

                    int neighbors = universe.getNumNeighbors(i, j, toroidal);

                    Cell futureCell = scratchpad.cells[i, j];

                    //If the number of neighbors is 3, dead or alive, the cell will be alive in the next generation
                    if (neighbors == 3) {
                        futureCell.state = true;
                        futureCell.generation = generation;
                    }

                    //If the number of neighbors is 2 and the cell is alive, the cell will stay alive in the next generation
                    if (presentCell.state == true && neighbors == 2) {
                        futureCell.state = true;
                        futureCell.generation = generation;
                    }
                }
            }

            //Copies the scratchpad to the universe
            for (int i = 0; i < w; i++) {
                for (int j = 0; j < h; j++) {
                    universe.cells[i, j] = scratchpad.cells[i, j];
                }
            }

            //Increments the number of passed generations
            generation++;
        }

        private void randomizeUniverse(Random rand) {
            for (int i = 0; i < w; i++) {
                for (int j = 0; j < h; j++) {
                    Cell cell = universe.cells[i, j];

                    //Randomly generates a value of either 0 or 1 and converts it into a boolean where true (1) is alive and false (0) is dead
                    if (Convert.ToBoolean(rand.Next(2))) {
                        cell.state = true;
                    } else {
                        cell.state = false;
                    }
                }
            }

            //Resets the generation count to 0
            generation = 0;
        }

        #endregion

        #region Controls

        private void timer_Tick(object sender, EventArgs e) {
            //Checks if the game was paused or unpaused
            checkForPause();

            info.Text = "Generation: " + generation + " Total Number of Cells: " + universe.getNumCells() + " Current Seed: " + seed;

            //Flags the graphics panel to be redrawn
            graphicsPanel.Invalidate();
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e) {
            inMenu = true;
        }

        private void menuStrip1_MenuDeactivate(object sender, EventArgs e) {
            inMenu = false;
        }

        private void open_Click(object sender, EventArgs e) {
            inMenu = true;

            if (openFileDialog.ShowDialog() == DialogResult.OK) {
                string[] rows = File.ReadAllLines(openFileDialog.FileName);

                //Checks if the file is a seeded universe or not
                if (rows[0].IndexOf('-') == -1 && rows[0].IndexOf('.') == -1) {
                    //Sets the universe's size to the one specified in the file
                    w = int.Parse(rows[1]);
                    h = int.Parse(rows[2]);
                    universe = new World(w, h);

                    //Generates the universe based on the seed given in the file
                    seed = int.Parse(rows[0]);
                    randomizeUniverse(new Random(seed));
                } else {
                    //Sets the universe's size to the one specified in the file
                    w = rows[0].Length;
                    h = rows.Length;
                    universe = new World(w, h);

                    for (int i = 0; i < h; i++) {
                        for (int j = 0; j < rows[i].Length; j++) {
                            Cell cell = universe.cells[j, i];

                            //Dashes denote alive cells while periods denote dead cells
                            if (rows[i][j] == '-') {
                                cell.state = true;
                            } else {
                                cell.state = false;
                            }
                        }
                    }
                }

                //Stores the current universe into the cache
                storeIntoCache();

                paused = true;
            }

            inMenu = false;
        }

        private void save_Click(object sender, EventArgs e) {
            inMenu = true;

            if (saveFileDialog.ShowDialog() == DialogResult.OK) {
                StreamWriter file = new StreamWriter(saveFileDialog.FileName);

                //Checks if the universe is seeded
                if (seeded) {
                    //Writes the seed, width, and height into the file
                    file.WriteLine(seed);
                    file.WriteLine(w);
                    file.WriteLine(h);
                } else {
                    //Splits the universe string into rows
                    string[] rows = universe.ToString().Split('|');

                    //Writes each row line by line into the file
                    foreach (string s in rows) {
                        //Checks if the last row has been reached
                        if (s != "") {
                            file.WriteLine(s);
                        }
                    }
                }

                file.Close();

                paused = true;
            }

            inMenu = false;
        }

        private void import_Click(object sender, EventArgs e) {
            inMenu = true;

            if (importFileDialog.ShowDialog() == DialogResult.OK) {
                List<string> rows = File.ReadAllLines(importFileDialog.FileName).ToList();

                //Locates the starting point of the world in the file
                int start = rows.IndexOf("!") + 1;

                //Calculates the width and height of the world in the file
                int w = rows[start].Length;
                int h = rows.Count - start;

                //Only resizes the universe if it's not big enough
                if (this.w < w) {
                    this.w = w;
                }

                if (this.h < h) {
                    this.h = h;
                }

                universe = new World(this.w, this.h);

                for (int i = start; i < rows.Count; i++) {
                    for (int j = 0; j < rows[i].Length; j++) {
                        Cell cell = universe.cells[j, i - start];

                        //Os denote alive cells while periods denote dead cells
                        if (rows[i][j] == 'O') {
                            cell.state = true;
                        } else {
                            cell.state = false;
                        }
                    }

                }

                //Stores the current universe into the cache
                storeIntoCache();

                paused = true;
            }

            inMenu = false;
        }

        private void quit_Click(object sender, EventArgs e) {
            Application.Exit();
        }

        private void options_Click(object sender, EventArgs e) {
            inMenu = true;

            OptionsDialog dialog = new OptionsDialog();

            //Sets the event handler for the apply button
            dialog.apply += applyEvent;

            if (dialog.ShowDialog() == DialogResult.OK) {
                //Gets the new settings from the options menu
                getSettings();
                //Checks if the resizable setting was changed
                checkForResize();
            }

            inMenu = false;
        }

        private void clear_Click(object sender, EventArgs e) {
            //Creates a new blank universe to clear it
            universe = new World(w, h);
            //Resets the generation count to 0
            generation = 0;
        }

        private void reset_Click(object sender, EventArgs e) {
            restoreFromCache();
        }

        private void randomizeTime_Click(object sender, EventArgs e) {
            //Randomizes the universe using the current time as a seed
            randomizeUniverse(new Random());
            //Flags the universe as non-seeded
            seeded = false;
            //Stores the current universe into the cache
            storeIntoCache();
        }

        private void randomizeCurrentSeed_Click(object sender, EventArgs e) {
            //Randomizes the universe using the currently set seed
            randomizeUniverse(new Random(seed));
            //Flags the universe as seeded
            seeded = true;
            //Stores the current universe into the cache
            storeIntoCache();
        }

        private void randomizeNewSeed_Click(object sender, EventArgs e) {
            NewSeedDialog dialog = new NewSeedDialog();
            dialog.seed = this.seed;

            if (dialog.ShowDialog() == DialogResult.OK) {
                //Sets the seed to whatever value was set in the dialog
                this.seed = dialog.seed;
                randomizeUniverse(new Random(seed));
            }

            //Flags the universe as seeded
            seeded = true;
            //Stores the current universe into the cache
            storeIntoCache();
        }

        private void run_ButtonClick(object sender, EventArgs e) {
            paused = false;
        }

        private void runTo_Click(object sender, EventArgs e) {
            RunToDialog dialog = new RunToDialog();
            dialog.minimum = this.generation + 1;

            if (dialog.ShowDialog() == DialogResult.OK) {
                runToGeneration = dialog.generation;

                //Checks if the user checked the run in real-time check box
                if (dialog.realTime) {
                    paused = false;
                } else {
                    //Instantaneously advances to the generation the user inputted
                    for (int i = generation; i < runToGeneration; i++) {
                        advanceGeneration();
                    }

                    runToGeneration = 0;
                }
            }
        }

        private void pause_Click(object sender, EventArgs e) {
            paused = true;
        }

        private void next_Click(object sender, EventArgs e) {
            //Advances the world to the next generation
            advanceGeneration();
        }

        private void about_Click(object sender, EventArgs e) {
            inMenu = true;

            AboutBox a = new AboutBox();
            a.ShowDialog();

            inMenu = false;
        }

        private void graphicsPanel_MouseClick(object sender, MouseEventArgs e) {
            if (paused) {
                //Checks for the left mouse button being clicked
                if (e.Button == MouseButtons.Left) {
                    Cell cell;

                    //Calls respective functions to check if a cell's state should be changed
                    if (resizable) {
                        cell = clickResizableGrid(e);
                    } else {
                        cell = clickStaticGrid(e);
                    }

                    //If the cell returned as null, the user didn't click inside a cell
                    if (cell != null) {
                        //Reverses the cell's state
                        cell.state = !cell.state;
                    }

                    //Flags the universe as non-seeded
                    seeded = false;
                    //Stores the current universe into the cache
                    storeIntoCache();
                //Checks for the right mouse button being clicked
                } else if (e.Button == MouseButtons.Right) {
                    Cell cell;

                    //Calls respective functions to get the cell object
                    if (resizable) {
                        cell = clickResizableGrid(e);
                    } else {
                        cell = clickStaticGrid(e);
                    }

                    int generation = 0;

                    //If the cell returned as null, the user didn't click inside a cell
                    if (cell != null) {
                        generation = cell.generation;
                    }

                    //Updates the context menu to show the clicked cell's life span
                    contextMenuStrip.Items[0].Text = "Generations Alive: " + generation;
                    contextMenuStrip.Show(graphicsPanel, e.X, e.Y);
                }
            }
        }

        #endregion

        #region UI

        private void checkForPause() {
            //If every cell dies or the run to condition is met, pause the game
            if (universe.isEmpty() || (runToGeneration != 0 && runToGeneration == generation)) {
                paused = true;
                runToGeneration = 0;
            }

            if (paused) {
                pause.Enabled = false;

                if (!cacheEmpty) {
                    reset.Enabled = true;
                }
                
                randomize.Enabled = true;
                snake.Enabled = true;

                //Enables buttons only if the univserse isn't empty
                if (universe.isEmpty()) {
                    clear.Enabled = false;
                    run.Enabled = false;
                    next.Enabled = false;
                } else {
                    clear.Enabled = true;
                    run.Enabled = true;
                    next.Enabled = true;
                }
            } else if (!inMenu) {
                pause.Enabled = true;

                clear.Enabled = false;
                reset.Enabled = false;
                randomize.Enabled = false;
                run.Enabled = false;
                next.Enabled = false;

                snake.Enabled = false;

                //Runs the game if the game is unpaused
                advanceGeneration();
            }
        }

        private void restoreFromCache() {
            universe = new World(w, h);

            for (int i = 0; i < w; i++) {
                for (int j = 0; j < h; j++) {
                    //Copies the cells from the cache into the universe
                    universe.cells[i, j] = cache.cells[i, j];
                }
            }
        }

        private void storeIntoCache() {
            if (cacheEmpty) {
                cacheEmpty = false;
            }

            cache = new World(w, h);

            for (int i = 0; i < w; i++) {
                for (int j = 0; j < h; j++) {
                    //Copies the cells from the universe into the cache
                    cache.cells[i, j] = universe.cells[i, j];
                }
            }
        }

        private void checkForResize() {
            //Sets the window to be resizable or not depending on the setting
            if (resizable) {
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.MaximizeBox = true;
            } else {
                this.FormBorderStyle = FormBorderStyle.FixedSingle;
                this.MaximizeBox = false;
                this.WindowState = FormWindowState.Normal;
            }
        }

        private void applyEvent() {
            //Gets the new settings from the options menu
            getSettings();
            //Checks if the resizable setting was changed
            checkForResize();
        }

        private void getSettings() {
            resizable = s.resizable;
            cellSize = s.cellSize;

            //Clears the scratchpad
            scratchpad = new World(w, h);

            //Copies the universe to the scratchpad
            for (int i = 0; i < w; i++) {
                for (int j = 0; j < h; j++) {
                    scratchpad.cells[i, j] = universe.cells[i, j];
                }
            }

            w = s.width;
            h = s.height;

            //Creates a resized universe
            universe = new World(w, h);

            int _w;
            int _h;

            //Checks if the new width is greater than the old width
            if (w > scratchpad.cells.GetLength(0)) {
                _w = scratchpad.cells.GetLength(0);
            } else {
                _w = w;
            }

            //Checks if the new height is greater than the old height
            if (h > scratchpad.cells.GetLength(1)) {
                _h = scratchpad.cells.GetLength(1);
            } else {
                _h = h;
            }

            //Copies the scratchpad to the universe
            for (int i = 0; i < _w; i++) {
                for (int j = 0; j < _h; j++) {
                    universe.cells[i, j] = scratchpad.cells[i, j];
                }
            }

            storeIntoCache();

            penSize = s.penSize;

            toroidal = s.toroidal;

            cellColor = s.cellColor;
            backgroundColor = s.backgroundColor;
            gridColor = s.gridColor;
            textColor = s.textColor;

            //Checks if fps was selected for the game speed setting
            if (s.fps) {
                timer.Interval = 1000 / s.speed;
            } else {
                timer.Interval = s.speed;
            }
        }

        #endregion

        #region Snake

            #region Global Variables

            //Game logic variables
            SnakeGame game;
            bool gamePaused;
            bool gameInMenu;
            SnakeGame.Direction input;

            //Graphics variables
            Color snakeColor;
            Color powerUpColor;

            float gameGraphicsWidth;
            float gameGraphicsHeight;
            float gameSegmentWidth;
            float gameSegmentHeight;

            int gameStaticSegmentSize;

            #endregion

            #region Initialization

            private void snake_ButtonClick(object sender, EventArgs e) {
                //Stops the loop for the game of life
                timer.Stop();
                graphicsPanel.Visible = false;

                menuStrip.Enabled = false;

                clear.Enabled = false;
                reset.Enabled = false;
                randomize.Enabled = false;
                run.Enabled = false;
                pause.Enabled = false;
                next.Enabled = false;

                pauseGame.Enabled = true;
                exitGame.Enabled = true;

                game = new SnakeGame(w, h);
                gamePaused = false;
                gameInMenu = false;
                
                //Initialization of the game timer
                gameTimer.Interval = 1000 / 15;
                gameTimer.Start();

                graphicsPanelSnake.BackColor = backgroundColor;
                graphicsPanelSnake.Visible = true;
            }

            #endregion

            #region Update Loop

            private void gameTimer_Tick(object sender, EventArgs e) {
                //Pauses the game if the user paused it or is in a menu
                if (!gamePaused && !gameInMenu) {
                    if (game.gameOver) {
                        gameOver();
                    } else {
                        game.moveSnake(input, toroidal);

                        //Displays the current score in the info strip
                        info.Text = "Score: " + game.score;

                        //Flags the graphics panel to be redrawn
                        graphicsPanelSnake.Invalidate();
                    }
                }
            }

            private void Form1_KeyDown(object sender, KeyEventArgs e) {
                //Retrieves keyboard input
                if (game != null) {
                    switch (e.KeyCode) {
                        case Keys.Up:
                            input = SnakeGame.Direction.Up;
                            break;
                        case Keys.Down:
                            input = SnakeGame.Direction.Down;
                            break;
                        case Keys.Left:
                            input = SnakeGame.Direction.Left;
                            break;
                        case Keys.Right:
                            input = SnakeGame.Direction.Right;
                            break;
                    }
                }
            }

            private void gameOver() {
                //Stops the game from running
                gameTimer.Stop();
                info.Text = "Game Over! Final Score: " + game.score + " Click the play button to play again or the exit button to quit.";
                pauseGame.Enabled = false;
            }

            #endregion

            #region Draw Loop

            private void graphicsPanelSnake_Paint(object sender, PaintEventArgs e) {
                if (game != null) {
                    //Updates the graphics variables
                    gameGraphicsWidth = graphicsPanelSnake.Width - penSize;
                    gameGraphicsHeight = graphicsPanelSnake.Height - penSize;

                    //Checks for if the grid is set to visible in the view menu and updates the graphics variables accordingly
                    if (grid.Checked) {
                        gameSegmentWidth = gameGraphicsWidth / w;
                        gameSegmentHeight = gameGraphicsHeight / h;
                        gameStaticSegmentSize = cellSize + penSize;
                    } else {
                        gameSegmentWidth = (float)graphicsPanelSnake.Width / w;
                        gameSegmentHeight = (float)graphicsPanelSnake.Height / h;
                        gameStaticSegmentSize = cellSize;
                    }

                    //Makes accessing e.Graphics simpler
                    Graphics g = e.Graphics;

                    //Initialization of the brush object that's used to draw the snake
                    Brush powerUpBrush = new SolidBrush(powerUpColor);

                    //Calls respective functions to draw the snake
                    if (resizable) {
                        drawResizablePowerUp(g, powerUpBrush);
                    } else {
                        drawStaticPowerUp(g, powerUpBrush);
                    }

                    //Clean up of the brush object
                    powerUpBrush.Dispose();

                    //Initialization of the brush object that's used to draw the snake
                    Brush snakeBrush = new SolidBrush(snakeColor);

                    //Calls respective functions to draw the snake
                    if (resizable) {
                        drawResizableSnake(g, snakeBrush);
                    } else {
                        drawStaticSnake(g, snakeBrush);
                    }

                    //Clean up of the brush object
                    snakeBrush.Dispose();

                    //Checks for if the grid is set to visible in the view menu
                    if (grid.Checked) {
                        //Initialization of the grid pen
                        Pen gridPen = new Pen(gridColor, this.penSize);
                        //Creation of an offset to draw the outline of the grid in the correct place
                        int penSizeOffset = this.penSize / 2;

                        //Calls respective functions to draw the grid
                        if (resizable) {
                            drawResizableGridGame(g, gridPen, penSizeOffset);
                        } else {
                            drawStaticGridGame(g, gridPen, penSizeOffset);
                        }

                        //Clean up of the pen object
                        gridPen.Dispose();
                    }
                }
            }

            #endregion

            #region Draw Grid

            private void drawResizableGridGame(Graphics g, Pen gridPen, int penSizeOffset) {
                //Draws the outline of the grid
                g.DrawRectangle(gridPen, penSizeOffset, penSizeOffset, gameGraphicsWidth, gameGraphicsHeight);

                //Draws the vertical lines of the grid
                for (int i = 1; i < w; i++) {
                    float x = (gameSegmentWidth * i) + penSizeOffset;
                    float y = gameGraphicsHeight;

                    g.DrawLine(gridPen, x, penSize, x, y);
                }

                //Draws the horizontal lines of the grid
                for (int j = 1; j < h; j++) {
                    float x = gameGraphicsWidth;
                    float y = (gameSegmentHeight * j) + penSizeOffset;

                    g.DrawLine(gridPen, penSize, y, x, y);
                }
            }

            private void drawStaticGridGame(Graphics g, Pen gridPen, int penSizeOffset) {
                //Sets constants to allow for translation from graphics panel dimensions to window dimensions
                const int widthOffset = 16;
                const int heightOffset = 110;

                int gameGraphicsWidth = gameStaticSegmentSize * w + penSize;
                int gameGraphicsHeight = gameStaticSegmentSize * h + penSize;

                //Scales the window according to the cell size and the dimensions of the grid from options
                this.Size = new Size(gameGraphicsWidth + widthOffset, gameGraphicsHeight + heightOffset);

                gameGraphicsWidth = graphicsPanelSnake.Width - penSize;
                gameGraphicsHeight = graphicsPanelSnake.Height - penSize;

                //Draws the outline of the grid
                g.DrawRectangle(gridPen, penSizeOffset, penSizeOffset, gameGraphicsWidth, gameGraphicsHeight);

                //Draws the vertical lines of the grid
                for (int i = 1; i < w; i++) {
                    int x = (gameStaticSegmentSize * i) + penSizeOffset;
                    int y = gameGraphicsHeight;

                    g.DrawLine(gridPen, x, penSize, x, y);
                }

                //Draws the horizontal lines of the grid
                for (int j = 1; j < h; j++) {
                    int x = gameGraphicsWidth;
                    int y = (gameStaticSegmentSize * j) + penSizeOffset;

                    g.DrawLine(gridPen, penSize, y, x, y);
                }
            }

            #endregion

            #region Draw Snake

            private void drawResizableSnake(Graphics g, Brush snakeBrush) {
                foreach (int[] s in game.snake) {
                    //Uses Float Rectangles to fill in the shape of the snake
                    RectangleF segment = new RectangleF(gameSegmentWidth * s[0], gameSegmentHeight * s[1], gameSegmentWidth, gameSegmentHeight);
                    g.FillRectangle(snakeBrush, segment);
                }
            }

            private void drawStaticSnake(Graphics g, Brush snakeBrush) {
                foreach (int[] s in game.snake) {
                    //Uses Rectangles to fill in the shape of the snake
                    Rectangle segment = new Rectangle(gameStaticSegmentSize * s[0], gameStaticSegmentSize * s[1], gameStaticSegmentSize, gameStaticSegmentSize);
                    g.FillRectangle(snakeBrush, segment);
                }
            }

            #endregion

            #region Draw Power-Up

            private void drawResizablePowerUp(Graphics g, Brush powerUpBrush) {
                //Uses a Float Rectangle to fill in the square for the power-up
                RectangleF powerUp = new RectangleF(gameSegmentWidth * game.powerUp[0], gameSegmentHeight * game.powerUp[1], gameSegmentWidth, gameSegmentHeight);
                g.FillRectangle(powerUpBrush, powerUp);
            }

            private void drawStaticPowerUp(Graphics g, Brush powerUpBrush) {
                //Resizes the window if view grid is unchecked
                if (!grid.Checked) {
                    //Sets constants to allow for translation from graphics panel dimensions to window dimensions
                    const int widthOffset = 16;
                    const int heightOffset = 110;

                    int gameGraphicsWidth = staticCellSize * w;
                    int gameGraphicsHeight = staticCellSize * h;

                    //Scales the window according to the cell size and the dimensions of the grid from options
                    this.Size = new Size(gameGraphicsWidth + widthOffset, gameGraphicsHeight + heightOffset);
                }

                //Uses a Rectangle to fill in the square for the power-up
                Rectangle powerUp = new Rectangle(gameStaticSegmentSize * game.powerUp[0], gameStaticSegmentSize * game.powerUp[1], gameStaticSegmentSize, gameStaticSegmentSize);
                g.FillRectangle(powerUpBrush, powerUp);
            }

        #endregion

            #region Controls

            private void snake_DropDownOpened(object sender, EventArgs e) {
                gameInMenu = true;

                //Checks if the colors are still set to their default values
                if (snakeColor == Color.Yellow && powerUpColor == Color.Orange) {
                    resetDefaultColors.Enabled = false;
                } else {
                    resetDefaultColors.Enabled = true;
                }
            }

            private void snake_DropDownClosed(object sender, EventArgs e) {
                gameInMenu = false;
            }

            private void pauseGame_Click(object sender, EventArgs e) {
                //Switches around the pause state of the game
                gamePaused = !gamePaused;
            }

            private void setSnakeColor_Click(object sender, EventArgs e) {
                gameInMenu = true;

                colorDialog.Color = snakeColor;

                //Gets and stores the new color of the snake into settings
                if (colorDialog.ShowDialog() == DialogResult.OK) {
                    snakeColor = colorDialog.Color;
                    setSnakeColor.BackColor = snakeColor;
                    s.snakeColor = snakeColor;
                    s.Save();
                }

                gameInMenu = false;
            }

            private void setPowerUpColor_Click(object sender, EventArgs e) {
                gameInMenu = true;

                colorDialog.Color = powerUpColor;

                //Gets and stores the new color of power-ups into settings
                if (colorDialog.ShowDialog() == DialogResult.OK) {
                    powerUpColor = colorDialog.Color;
                    setPowerUpColor.BackColor = powerUpColor;
                    s.powerUpColor = powerUpColor;
                    s.Save();
                }

                gameInMenu = false;
            }

            private void resetDefaultColors_Click(object sender, EventArgs e) {
                //Resets the colors back to their default values and updates settings with these values
                snakeColor = Color.Yellow;
                setSnakeColor.BackColor = snakeColor;
                s.snakeColor = snakeColor;

                powerUpColor = Color.Orange;
                setPowerUpColor.BackColor = powerUpColor;
                s.powerUpColor = powerUpColor;

                s.Save();
            }

            private void quitGame_Click(object sender, EventArgs e) {
                graphicsPanelSnake.Visible = false;

                menuStrip.Enabled = true;

                pauseGame.Enabled = false;
                exitGame.Enabled = false;

                //Starts the loop for game of life again
                timer.Start();
                graphicsPanel.Visible = true;
            }

            #endregion

        #endregion
    }
}
