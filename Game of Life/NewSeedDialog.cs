﻿using System;
using System.Windows.Forms;

namespace Game_of_Life {
    public partial class NewSeedDialog : Form {
        //Allows access to the numeric up down control from Form1
        public int seed {
            get {
                return (int)numericSeed.Value;
            }

            set {
                numericSeed.Value = value;
            }
        }

        public NewSeedDialog() {
            InitializeComponent();
        }

        private void buttonRandomSeed_Click(object sender, System.EventArgs e) {
            //Generates a random seed using the current time
            numericSeed.Value = new Random().Next();
        }
    }
}
