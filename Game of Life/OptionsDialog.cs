﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Game_of_Life {
    //Declaration of the custom apply event
    public delegate void ApplyEvent();

    public partial class OptionsDialog : Form {
        #region Global Variables
        
        //Initialization of the apply event
        public event ApplyEvent apply;

        Game_of_Life.Properties.Settings s = Properties.Settings.Default;

        //Initialization of the variables to control the fps visual
        int x = 0;
        bool right = true;

        #endregion

        #region Initialization

        public OptionsDialog() {
            InitializeComponent();

            //Initialization of the controls with the values in settings
            setControls(s.resizable, s.cellSize, s.width, s.height, s.penSize, s.fps, s.speed, s.toroidal, s.cellColor, s.backgroundColor, s.gridColor, s.textColor);

            //Initialization of the timer
            if (s.fps) {
                timer.Interval = 1000 / s.speed;
            } else {
                timer.Interval = s.speed;
            }

            timer.Start();
        }

        #endregion

        #region Controls

        private void buttonReset_Click(object sender, EventArgs e) {
            setControls(true, 20, 25, 25, 1, true, 90, true, Color.Gray, Color.White, Color.Black, Color.Black);
        }

        private void buttonOK_Click(object sender, EventArgs e) {
            saveSettings();
        }

        private void buttonApply_Click(object sender, EventArgs e) {
            saveSettings();
            //Calls the custom apply event
            apply?.Invoke();
        }

        private void buttonCellColor_Click(object sender, EventArgs e) {
            colorDialog.Color = buttonCellColor.BackColor;

            if (colorDialog.ShowDialog() == DialogResult.OK) {
                buttonCellColor.BackColor = colorDialog.Color;
            }
        }

        private void buttonBackgroundColor_Click(object sender, EventArgs e) {
            colorDialog.Color = buttonBackgroundColor.BackColor;

            if (colorDialog.ShowDialog() == DialogResult.OK) {
                buttonBackgroundColor.BackColor = colorDialog.Color;
            }
        }

        private void buttonGridColor_Click(object sender, EventArgs e) {
            colorDialog.Color = buttonGridColor.BackColor;

            if (colorDialog.ShowDialog() == DialogResult.OK) {
                buttonGridColor.BackColor = colorDialog.Color;
            }
        }

        private void buttonTextColor_Click(object sender, EventArgs e) {
            colorDialog.Color = buttonTextColor.BackColor;

            if (colorDialog.ShowDialog() == DialogResult.OK) {
                buttonTextColor.BackColor = colorDialog.Color;
            }
        }

        private void setControls(bool resizable, int cellSize, int width, int height, int penSize, bool fps, int speed, bool toroidal, Color cellColor, Color backgroundColor, Color gridColor, Color textColor) {
            checkResizable.Checked = resizable;
            numericCellSize.Value = cellSize;
            numericWidth.Value = width;
            numericHeight.Value = height;
            numericGridThickness.Value = penSize;

            radioFPS.Checked = fps;
            radioMilliseconds.Checked = !fps;

            if (fps) {
                numericFPS.Value = speed;
                numericMilliseconds.Value = 1000 / speed;
            } else {
                numericFPS.Value = 1000 / speed;
                numericMilliseconds.Value = speed;
            }

            comboBoundary.SelectedIndex = Convert.ToInt32(!toroidal);

            buttonCellColor.BackColor = cellColor;
            buttonBackgroundColor.BackColor = backgroundColor;
            buttonGridColor.BackColor = gridColor;
            buttonTextColor.BackColor = textColor;
        }

        private void saveSettings() {
            s.resizable = checkResizable.Checked;
            s.cellSize = (int)numericCellSize.Value;
            s.width = (int)numericWidth.Value;
            s.height = (int)numericHeight.Value;
            s.penSize = (int)numericGridThickness.Value;

            s.fps = radioFPS.Checked;

            if (s.fps) {
                s.speed = (int)numericFPS.Value;
            } else {
                s.speed = (int)numericMilliseconds.Value;
            }

            s.toroidal = !Convert.ToBoolean(comboBoundary.SelectedIndex);

            s.cellColor = buttonCellColor.BackColor;
            s.backgroundColor = buttonBackgroundColor.BackColor;
            s.gridColor = buttonGridColor.BackColor;
            s.textColor = buttonTextColor.BackColor;

            s.Save();

            if (s.fps) {
                timer.Interval = 1000 / s.speed;
            } else {
                timer.Interval = s.speed;
            }
        }

        private bool checkControlChanges(bool resizable, int cellSize, int width, int height, int penSize, bool fps, int speed, bool toroidal, Color cellColor, Color backgroundColor, Color gridColor, Color textColor) {
            if (checkResizable.Checked != resizable ||
                numericCellSize.Value != cellSize ||
                numericWidth.Value != width ||
                numericHeight.Value != height ||
                numericGridThickness.Value != penSize ||
                radioFPS.Checked != fps ||
                (fps && numericFPS.Value != speed) ||
                (!fps && numericMilliseconds.Value != speed) ||
                comboBoundary.SelectedIndex != Convert.ToInt32(!toroidal) ||
                buttonCellColor.BackColor != cellColor ||
                buttonBackgroundColor.BackColor != backgroundColor ||
                buttonGridColor.BackColor != gridColor ||
                buttonTextColor.BackColor != textColor) {
                return true;
            }

            return false;
        }

        #endregion

        #region UI

        private void checkResizable_CheckedChanged(object sender, EventArgs e) {
            //Changes the minimum and maximum values for width and height depending on if the window is set to resizable or not
            if (checkResizable.Checked) {
                labelCellSize.Enabled = false;
                numericCellSize.Enabled = false;

                numericWidth.Minimum = 5;
                numericWidth.Maximum = 200;
                numericHeight.Minimum = 5;
                numericHeight.Maximum = 200;
            } else {
                labelCellSize.Enabled = true;
                numericCellSize.Enabled = true;

                numericWidth.Minimum = 15;
                numericWidth.Maximum = 75;
                numericHeight.Minimum = 1;
                numericHeight.Maximum = 45;
            }
        }

        private void radioFPS_CheckedChanged(object sender, EventArgs e) {
            //Switches between setting the game speed using fps or milliseconds
            if (radioFPS.Checked) {
                numericFPS.Enabled = true;
                numericMilliseconds.Enabled = false;
            } else {
                numericFPS.Enabled = false;
                numericMilliseconds.Enabled = true;
            }
        }

        private void numericFPS_ValueChanged(object sender, EventArgs e) {
            //Dynamically calculates the millisecond equivalent of the current fps
            if (radioFPS.Checked) {
                numericMilliseconds.Value = 1000 / (int)numericFPS.Value;
            }
        }

        private void numericMilliseconds_ValueChanged(object sender, EventArgs e) {
            //Dynamically calculates the fps equivalent of the current number of milliseconds
            if (!radioFPS.Checked) {
                numericFPS.Value = 1000 / (int)numericMilliseconds.Value;
            }
        }

        private void timer_Tick(object sender, EventArgs e) {
            //Checks if the direction of the square needs to be switched or not
            if (x + 40 == graphicsPanel.Width) {
                right = false;
            } else if (x == 0) {
                right = true;
            }

            //Moves the square according to its direction
            if (right == true) {
                x += 10;
            } else {
                x -= 10;
            }

            //Enables or disables the apply button depending on if the current controls' values are the same as the values in settings
            buttonApply.Enabled = checkControlChanges(s.resizable, s.cellSize, s.width, s.height, s.penSize, s.fps, s.speed, s.toroidal, s.cellColor, s.backgroundColor, s.gridColor, s.textColor);
            //Enables or disables the reset button depending on if the current controls' values are the same as the default values
            buttonReset.Enabled = checkControlChanges(true, 20, 25, 25, 1, true, 90, true, Color.Gray, Color.White, Color.Black, Color.Black);

            graphicsPanel.Invalidate();
        }

        private void graphicsPanel1_Paint(object sender, PaintEventArgs e) {
            Brush squareBrush = new SolidBrush(s.cellColor);

            //Draws the fps visual
            Rectangle square = new Rectangle(x, 0, 40, 40);
            e.Graphics.FillRectangle(squareBrush, square);
        }

        #endregion
    }
}
